require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @activated_user = users(:mallory)
    @non_activated_user = users(:cyril)
  end

  test "should not see profile of unactivated users" do
    get user_path(@non_activated_user)
    assert_redirected_to root_url
  end

  test "should see profile of activated users" do
    get user_path(@activated_user)
    assert_response :success 
  end

  test "profile display" do
    get user_path(@user)
    assert_template 'users/show'
    assert_select 'title', full_title(@user.name)
    assert_select 'h1', text: @user.name
    assert_select 'h1>img.gravatar'
    assert_match @user.microposts.count.to_s, response.body
    assert_select 'div.pagination'
    @user.microposts.paginate(page: 1).each do |micropost|
      assert_match micropost.content, response.body
    end
  end
end
